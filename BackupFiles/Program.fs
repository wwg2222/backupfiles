﻿open System
open System.IO
open System.Threading
open System.Threading.Tasks
open System.Diagnostics

#if INTERACTIVE
#time
#endif


type FileUnit = 
    | FileElem of string * DirectoryInfo
    | DirectoryElem of string * DirectoryInfo * seq<FileUnit>


//version2
let rec getFiles path = 
    match Directory.Exists(path),File.Exists(path) with
    |false, true -> FileElem(path, DirectoryInfo(path))
    |true, false -> DirectoryElem(path, DirectoryInfo(path), 
                        Directory.GetFiles(path) |> Seq.collect (fun it -> [FileElem(it, DirectoryInfo(it))])
                        |> Seq.append <| 
                        (Directory.GetDirectories(path) |> Seq.collect (fun it -> [ it |> getFiles ])))
    |_           -> failwith("can not get file or directory")
                            
let rec getFileList fu (ignoreset:Set<string>)= 
    seq{
        match fu with
        |FileElem(name, info) -> yield name, info
        |DirectoryElem(name, info, lst) -> 
            if not (ignoreset.Contains(info.Name)) then
                yield name, info; yield! (lst |> Seq.collect  (fun x -> getFileList x ignoreset))
    }


let percentage (total:int64)= MailboxProcessor.Start(fun inbox->
    let rec loop count = async{
        let! msg = inbox.Receive()
        Console.Write("\r{0:F2}%", (float((count + msg)*10000L/total)/100.0))
        return! loop (count+msg)
        }
    loop 0L
    )


[<EntryPoint>]
let main argv = 
    let source,dest,ignorestr = argv.[0], argv.[1], argv.[2]
    let ignoreList = ignorestr.Split([|',';';'|])        
    let stopWatch = Stopwatch.StartNew()

    //get file list
    let funitlist = source |> getFiles |> getFileList <| (ignoreList |> Set.ofArray)//ignoreList
    let d,f = funitlist |> List.ofSeq |> List.partition (fun (_, a) -> a.Attributes &&& FileAttributes.Directory = FileAttributes.Directory) 
    //make destination folder
    let Today = DateTime.Now.Date
    let bkdest = dest.ToString() + "/"+ String.Format("{0}-{1}-{2}", Today.Year, Today.Month, Today.Day)
    //create directories
    for (path, _) in d do    
        Directory.CreateDirectory(path.Replace(source, bkdest)) |> ignore

    let filesize = f |> Seq.fold (fun state (x,_)  -> state + FileInfo(x).Length) 0L
    let p = percentage filesize

    f |> List.iter (fun (srcpath,_) -> 
                        File.Copy(srcpath, srcpath.Replace(source, bkdest))
                        p.Post(FileInfo(srcpath).Length))


    stopWatch.Stop()
    Thread.Sleep 2000
    printfn "\nSpend %fs\nEnter Any Key to End" stopWatch.Elapsed.Seconds

    Console.ReadKey()|>ignore
    0 // return an integer exit code


//version1
//let rec getFiles path = 
//    match Directory.Exists(path),File.Exists(path) with
//    |false, true -> FileElem(path, DirectoryInfo(path))
//    |true, false -> DirectoryElem(path, DirectoryInfo(path), 
//                        seq{for elem in Directory.GetFiles(path) do
//                                yield FileElem(elem, DirectoryInfo(elem));
//                            for elem in Directory.GetDirectories(path) do
//                                yield (elem |> getFiles)})
//    |_           -> failwith("get file)"


//let rec AsyncCopy(sourceFile:Stream, destinationFile:Stream, p:MailboxProcessor<bigint>) = 
//    async{
//        if sourceFile.Length <> 0L then
//            let buf = Array.zeroCreate 0x100000
//            let! count = sourceFile.AsyncRead(buf, 0, buf.Length)
//            do!  destinationFile.AsyncWrite(buf, 0, count)
//            p.Post (bigint count)
//            if count >0 then
//                return! AsyncCopy(sourceFile, destinationFile, p)
//    }      
//    //copy file
//    let AsyncCopyFile source dest =
//        async{
//              //Console.WriteLine("[.NET Thread {0}] {1}", Threading.Thread.CurrentThread.ManagedThreadId,source)
//              use stream_reader = File.OpenRead(source)
//              use stream_writer = File.OpenWrite(source.Replace(source, dest))
//              do! AsyncCopy(stream_reader, stream_writer, p)
//        }
//
//    [ for (src, _) in f ->
//        AsyncCopyFile src (src.Replace(source, bkdest))]
//    |> Async.Parallel
//    |> Async.RunSynchronously
//    |> ignore